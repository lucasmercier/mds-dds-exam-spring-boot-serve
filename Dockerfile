FROM openjdk:8

COPY ./target/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar

CMD ["java","-jar","mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar"]