# Spring Boot JPA MySQL - Building Rest CRUD API example

## Install
- Faites "java -version". Il faut que ce soit un JDK (et non un JRE). Si cela ne fonctionne pas, installez java : https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe
- Installer maven : https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip (unzip dans un répertoire de dev), ajouter la variable M2_HOME et ajouter au PATH (cf :https://howtodoinjava.com/maven/how-to-install-maven-on-windows/).
- Tester si maven fonctionne : "mvn --version"
- Installer Mysql 8 si ce n'est pas déjà fait
- lancer "mvn install" dans le répertoire
- Modifier le fichier : src/main/resources/application.properties



## Run Spring Boot application
```
mvnw spring-boot:run 
```

- Tester l'app : http://localhost:8080

Pour le docker file back : 

docker build -t image-back .

docker run --name image-tpnote-back -e SPRING_DATASOURCE_URL="jdbc:mysql://mds2.cdxwmy7n2r5p.eu-west-3.rds.amazonaws.com:3306/lucas_exam" -e SPRING_DATASOURCE_USERNAME=username -e SPRING_DATASOURCE_PASSWORD=password -p 8965:8080 image-back

Pour le docker compose :
docker compose up -d

J'ai des erreurs lorsque je run mes images back, je suppose que c'est parce que je n'ai jamais pu utiliser JDK (ayant deja un JRE que j'ai désinstallé environ 19 fois mais le pc n'a jamais voulu utiliser autre chose sachant que JRE n'est vraiment plus sur mon pc).
